Opis projektu:

Celem projektu było utworzenie aplikacji frontendowej.
Aplikacja ta powiązana jest z muzyką - wskazuje na to layout, lista piosenek dla każdego gatunku umieszczona w prawej części strony oraz miejsce z opisem każdego gatunku muzycznego.
Użytkownik wchodząc na stronę może dowiedzieć się wiecej o danym gatunku muzycznym oraz posłuchać jednej z piosenek z konkretnego gatunku muzycznego.
Lista w prawej części strony zawiera hiperlinki do odpowiednich stron aplikacji Spotify - do jednej z przykładowych piosenek danego wykonawcy z listy.
Menu znajdujące się po lewej stronie umożliwia zmianę gatunku muzycznego.
U samej góry aplikacji znajduje się nieruchomy header ze zdjęciem powiązanym z muzyką.

Funkcjonalności:
- Jest to aplikacja typu "single-page"
- Projekt zawiera 5 podstron (Home, Pop, Rock, Rap, Metal).
- Do zmiany treści strony zastosowałem HashRouter + NavLinki.
- Menu stworzone przy użyciu Accordiona oraz Card'ów.
- Użycie na stronie 2 zdjęć: w headerze oraz pod Menu.
- Użycie CSS'a.
- Lista piosenek (w prawej części) została utworzona przy użyciu InfiniteScrolling (na początku wczytywanych jest 8 artystów, potem jeszcze 3 x 4 artystów).
- Dane o wykonawcach i hiperlinki do utworów czytane są z plików .json znajdujących się w folderze "json".
- Możliwość dodania komentarzy w sekcji "Home" - zapisywane są w przeglądarkowej bazie danych, co rozumiem jako obecność komentarzy na stronie aż do jej odświeżenia.
Aby dodać komentarz, nick musi mieć długość z przedziału [3,15], a kometarz długość z przedziału [5,40].
Komentarze wyświetlają się nad formularzem i można je scrollować, gdy jest ich dużo.

Wykorzystane technologie: HTML5 + CSS3 + React.Js + troszkę Boostrapa i React-Bootstrapa (do Accordiona i Card'ów)

Aby korzystać z aplikacji wystarczy wykonać polecenia: npm install oraz npm start, aplikacja działa na porcie 3000.

Rafał Brożek.
