import React from 'react';
import ReactDOM from 'react-dom';
import Menu from './scripts/Menu';
import Header from "./scripts/Header";
import Content from "./scripts/Content";
import List from "./scripts/List";
import Comments from "./scripts/Comments";
import Image from "./scripts/NotesImage";

ReactDOM.render(<Image/>, document.getElementById('notes'));
ReactDOM.render(<Header/>, document.getElementById('header'));
ReactDOM.render(<Menu/>, document.getElementById('menu'));
ReactDOM.render(<Content/>, document.getElementById('content'));
ReactDOM.render(<List/>, document.getElementById('list'));
ReactDOM.render(<Comments/>, document.getElementById('listOfComments'));

