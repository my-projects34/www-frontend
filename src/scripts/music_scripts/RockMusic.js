import List from "./List";
import React from "react";

class RockList extends React.Component {
    render() {
        return (<List genre={"rock"}/>);
    }
}

export default RockList
