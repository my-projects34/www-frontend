import List from "./List";
import React from "react";

class PopList extends React.Component {

    render() {
        return (<List genre={"pop"}/>);
    }
}

export default PopList
