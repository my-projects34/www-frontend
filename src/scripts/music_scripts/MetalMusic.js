import List from "./List";
import React from "react";

class MetalList extends React.Component {

    render() {
        return (<List genre={"metal"}/>);
    }
}

export default MetalList
