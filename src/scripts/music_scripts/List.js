import React from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import jsonFilePop from "../../json/pop";
import jsonFileMetal from "../../json/metal";
import jsonFileRap from "../../json/rap";
import jsonFileRock from "../../json/rock";
import '../../styles/style.css';


var jsonFile = jsonFilePop;

let foo = [];

for (let i = 0; i < 8; i++) {
    foo.push(null);
}

function getNextArtists(length) {
    let nextArtists = [];
    for (let i = length; i < length + 4; i++) {
        nextArtists.push(jsonFile[i]);
    }
    return nextArtists;
}

class List extends React.Component {
    constructor(props) {
        super(props);
        switch (this.props.genre) {
            case "pop":
                jsonFile = jsonFilePop;
                break;
            case "rock":
                jsonFile = jsonFileRock;
                break;
            case "metal":
                jsonFile = jsonFileMetal;
                break;
            case "rap":
                jsonFile = jsonFileRap;
                break;
            default:
                jsonFile = jsonFilePop;
                break;
        }
    }

    changeDisplay() {
        let list = document.getElementById("list");
        list.style.display = "block";
        let listComments = document.getElementById("listOfComments");
        listComments.style.display = "none";
    }

    state = {
        items: Array.from(foo),
        hasMore: true,
    };

    fetchMoreData = () => {
        if (this.state.items.length >= 20) {
            this.setState({hasMore: false});
            return;
        }
        setTimeout(() => {
            this.setState({
                items: this.state.items.concat(Array.from(getNextArtists(this.state.items.length)))
            });
        }, 1000);
    };

    render() {
        this.changeDisplay();
        return (
            <div>
                <hr/>
                <InfiniteScroll
                    scrollableTarget={"list"}
                    next={this.fetchMoreData}
                    dataLength={this.state.items.length}
                    hasMore={this.state.hasMore}
                    loader={<h3>Ładowanie...</h3>}>

                    {this.state.items.map((i, index) => (
                        <a key={index} href={jsonFile[index]['link']} target={"_blank"}>
                            <div className="artists" key={index}>
                                {jsonFile[index]['number']} - {jsonFile[index]['name']} {jsonFile[index]['surname']}
                            </div>
                        </a>
                    ))}
                </InfiniteScroll>
            </div>
        );
    }
}

export default List
