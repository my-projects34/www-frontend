import React from 'react';
import Accordion from 'react-bootstrap/Accordion'
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import ListGroup from 'react-bootstrap/ListGroup'
import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/style.css';
import {
    NavLink,
    HashRouter
} from "react-router-dom";

function Menu() {
    return (
        <div>
            <Accordion>
                <Card>
                    <Card.Header style={{backgroundColor: '#e1dbd5', borderColor: '#333'}}>
                        <Accordion.Toggle as={Button} variant="link" eventKey="0">
                            Menu
                        </Accordion.Toggle>
                    </Card.Header>
                    <Accordion.Collapse eventKey="0">
                        <Card.Body style={{backgroundColor: '#e1dbd5', borderColor: '#333'}}>
                            <ListGroup variant="flush">
                                <HashRouter>
                                    <ListGroup.Item className="listItem" ><NavLink
                                        exact
                                        to="/">Home</NavLink></ListGroup.Item>
                                    <ListGroup.Item className="listItem"><NavLink
                                        to="/pop">Pop</NavLink></ListGroup.Item>
                                    <ListGroup.Item className="listItem"><NavLink
                                        to="/rap">Rap</NavLink></ListGroup.Item>
                                    <ListGroup.Item className="listItem"><NavLink
                                        to="/rock">Rock</NavLink></ListGroup.Item>
                                    <ListGroup.Item className="listItem"><NavLink
                                        to="/metal">Metal</NavLink></ListGroup.Item>
                                </HashRouter>
                            </ListGroup>
                        </Card.Body>
                    </Accordion.Collapse>
                </Card>
            </Accordion>
        </div>
    );
}

export default Menu;
