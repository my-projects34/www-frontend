import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import "../styles/style.css"
import {
    Route,
    HashRouter
} from "react-router-dom";
import MetalMusic from './music_scripts/MetalMusic';
import PopMusic from './music_scripts/PopMusic';
import RapMusic from './music_scripts/RapMusic';
import RockMusic from './music_scripts/RockMusic';
import CleanStuff from './music_scripts/CleanStuff';

class List extends React.Component {
    render() {
        return (
            <HashRouter>
                <Route exact path="/" component={CleanStuff}/>
                <Route path="/pop" component={PopMusic}/>
                <Route path="/rock" component={RockMusic}/>
                <Route path="/metal" component={MetalMusic}/>
                <Route path="/rap" component={RapMusic}/>
            </HashRouter>
        )
    }
}

export default List
