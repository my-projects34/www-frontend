import React from 'react';
import notesImage from "../images/notes.png"


class NotesImage extends React.Component {
    render() {
        return (
            <div>
                {/* eslint-disable-next-line jsx-a11y/img-redundant-alt */}
                <img src={notesImage} alt={"Problem with loading the picture"} style={{height: 180, width: 200}}/>
            </div>
        )
    }
}

export default NotesImage
