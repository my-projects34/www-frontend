import React from "react";
import ReactDOM from "react-dom"


function generateComment(tab1, tab2) {
    return (<div>
        {(() => {
            const result = [];
            for (let i = 0; i < tab1.length; i++) {
                result.push(<div
                    style={{wordWrap: "break-word", marginLeft: 15, marginRight: 15, border: "3px solid #e1dbd5"}}
                    key={i}><span style={{color: "brown", fontSize: "larger"}}>Name:</span> {tab1[i]} <br/><span
                    style={{color: "brown", fontSize: "larger"}}>Comment:</span> <br/>{tab2[i]}</div>);
            }
            return result;
        })()}
    </div>);
}


class Comments extends React.Component {
    constructor(props) {
        super(props);
        this.state = {name: '', comment: '', tabOfNames: [], tabOfComments: []};

        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangeComment = this.handleChangeComment.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChangeName(event) {
        this.setState({name: event.target.value});
    }

    handleChangeComment(event) {
        this.setState({comment: event.target.value});
    }

    handleSubmit(event) {
        if (this.state.comment.length >= 3 && this.state.comment.length <= 40 && this.state.name.length >= 3 && this.state.name.length <= 15) {
            this.state.tabOfNames.push(this.state.name);
            this.state.tabOfComments.push(this.state.comment);
            ReactDOM.render(generateComment(this.state.tabOfNames, this.state.tabOfComments), document.getElementById("comments"));
        }

        event.preventDefault();
    }

    render() {
        return (
            <div>
                <div id="comments" style={{height: 300, overflow: "auto"}}/>
                <form onSubmit={this.handleSubmit} style={{borderBlockColor: "bisque"}}>
                    <br/>
                    Dodaj komentarz do strony!
                    <br/>
                    <label>
                        Podaj nick:
                        <br/>
                        <input type="text" minLength={"3"} maxLength={"15"} value={this.state.name}
                               onChange={this.handleChangeName}/>
                    </label>
                    <label>
                        Podaj treść komentarza:
                        <br/>
                        <textarea minLength={"3"} maxLength={"40"} value={this.state.comment}
                                  onChange={this.handleChangeComment}/>
                    </label>
                    <br/>
                    <input type="submit" value="Wyślij"/>
                </form>
            </div>
        );
    }
}

export default Comments
