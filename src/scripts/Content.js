import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import "../styles/style.css"
import {
    Route,
    HashRouter
} from "react-router-dom";
import HomeContent from './contents/HomeContent';
import PopContent from './contents/PopContent';
import RockContent from './contents/RockContent';
import MetalContent from './contents/MetalContent';
import RapContent from './contents/RapContent';

class Content extends React.Component {
    render() {
        return (
            <HashRouter>
                <Route exact path="/" component={HomeContent}/>
                <Route path="/pop" component={PopContent}/>
                <Route path="/rock" component={RockContent}/>
                <Route path="/metal" component={MetalContent}/>
                <Route path="/rap" component={RapContent}/>
            </HashRouter>
        )
    }
}

export default Content
