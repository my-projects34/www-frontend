import React from "react";

class MetalContent extends React.Component {
    render() {
        return (
            <div>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ultrices augue vitae eros placerat
                    suscipit. Proin eu nulla sed neque suscipit faucibus eget cursus massa. Nam laoreet enim et molestie
                    porttitor. Sed pharetra, quam at tempus tempus, augue urna tristique risus, eget lacinia tortor eros
                    non
                    ex. Donec id augue eu metus volutpat pellentesque. Etiam ac nisi et risus tincidunt sollicitudin a
                    et
                    turpis. Praesent pellentesque nisi in turpis pharetra facilisis. Proin consectetur mi elit, in
                    varius
                    justo faucibus ac. Aliquam interdum vitae arcu non porttitor. Curabitur in massa ut est luctus
                    tincidunt
                    id non odio.</p>

                <p>Quisque laoreet mollis dui sit amet condimentum. Sed vehicula elit turpis, sed suscipit sem
                    pellentesque
                    non. Duis a blandit nibh. Nam tincidunt consequat aliquam. Class aptent taciti sociosqu ad litora
                    torquent per conubia nostra, per inceptos himenaeos. Mauris lacinia eleifend ipsum sit amet finibus.
                    Duis justo nisi, convallis tempus urna id, tincidunt mattis ipsum. Class aptent taciti sociosqu ad
                    litora torquent per conubia nostra, per inceptos himenaeos. Orci varius natoque penatibus et magnis
                    dis
                    parturient montes, nascetur ridiculus mus. Nullam placerat blandit enim, vel accumsan urna congue
                    eget.
                    Cras auctor faucibus ex non interdum.</p>

                <p>Quisque sit amet dolor ultricies, ullamcorper erat mattis, lobortis eros. Pellentesque feugiat odio
                    ac
                    mauris pulvinar ultrices. Praesent at pellentesque libero. Nulla facilisi. Mauris sollicitudin arcu
                    in
                    pulvinar venenatis. Vivamus congue sit amet magna vel convallis. Suspendisse venenatis orci sed dui
                    iaculis semper. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus
                    mus.
                    Nam tellus purus, pulvinar aliquam congue vitae, lacinia et ligula. Nulla eu lobortis risus. Fusce
                    rutrum luctus imperdiet. Maecenas leo tellus, dictum at mollis sodales, imperdiet in mauris.
                    Praesent
                    blandit lorem nisi, sed tristique dolor sollicitudin at. Maecenas sodales odio a neque efficitur,
                    sed
                    pretium turpis euismod. Mauris consequat sapien sed consequat vulputate.</p>

                <p>Nam leo ante, efficitur ac posuere sed, pretium sed odio. Vestibulum id quam velit. Curabitur et erat
                    vitae nisl tristique iaculis ac ac magna. Morbi accumsan enim tellus, quis sodales nisl laoreet
                    eget. Ut
                    porta turpis ut sem rutrum, vel vulputate massa vestibulum. Cras id justo at neque feugiat
                    tristique.
                    Donec at augue vel magna egestas dictum ut sed nisl. In eu gravida leo. Curabitur vel tellus lectus.
                    Nullam tincidunt sem sit amet tempus ornare. Integer vehicula laoreet dui eget ornare.</p>

                <p>Praesent nec ligula quam. Curabitur volutpat velit suscipit turpis fringilla, nec ultrices leo
                    varius.
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit aliquet nunc ut feugiat.
                    Aliquam
                    placerat orci nulla, nec dignissim felis fermentum ac. Mauris viverra erat lorem, tempor porttitor
                    justo
                    pretium id. Aliquam sit amet rhoncus risus. Vestibulum ante ipsum primis in faucibus orci luctus et
                    ultrices posuere cubilia Curae; Aliquam non egestas leo. Nullam iaculis arcu eget diam hendrerit,
                    nec
                    efficitur nisl fermentum.</p>

                <p>Quisque in aliquam sem. Donec aliquam egestas arcu et consequat. Cras ut nisi iaculis, lobortis arcu
                    nec, convallis turpis. Suspendisse vel dolor eleifend, fermentum lectus in, pulvinar urna. Praesent
                    eu
                    nibh scelerisque, facilisis nisi dapibus, gravida ante. Etiam et velit elit. In aliquet nec enim nec
                    porttitor. Sed convallis suscipit sagittis. Sed eros eros, dapibus tempor eleifend eu, tristique ut
                    magna. Nullam elit mauris, dictum quis libero sed, venenatis imperdiet dolor. Etiam eleifend tortor
                    enim.</p>

                <p>Mauris a nunc diam. Nullam nunc purus, viverra ac ligula nec, euismod tristique metus. Aenean in est
                    nec
                    dui dapibus iaculis ut sit amet nibh. Donec eu lorem posuere, interdum nibh ac, posuere enim. Fusce
                    commodo ex ac ante vehicula consectetur sed bibendum augue. Nunc vel ligula lacinia, maximus mauris
                    vitae, ultrices libero. Aenean auctor enim placerat tempor vulputate. Interdum et malesuada fames ac
                    ante ipsum primis in faucibus. Suspendisse potenti. Donec volutpat sem nisi, vitae mollis risus
                    pretium
                    a. Donec dapibus vel dui sit amet elementum. Orci varius natoque penatibus et magnis dis parturient
                    montes, nascetur ridiculus mus. Aenean auctor, neque eget vulputate dapibus, urna purus scelerisque
                    nisl, id sagittis ante purus eget ex. Mauris vel venenatis urna.</p>

                <p>Fusce dictum bibendum laoreet. Nam malesuada turpis libero, id ultrices mi faucibus at. Phasellus
                    suscipit id magna non malesuada. Phasellus accumsan libero vitae urna maximus tincidunt. Nullam
                    malesuada mi quis vulputate rhoncus. Nunc vulputate risus nec cursus luctus. Integer sit amet
                    sagittis
                    mi. Vestibulum vel rhoncus mi, at convallis ipsum. Aliquam id nulla malesuada nisl sollicitudin
                    euismod.
                    Duis efficitur tellus eget ullamcorper malesuada.</p>

                <p>Duis facilisis mi metus, luctus auctor ex condimentum sit amet. Suspendisse viverra erat quis lectus
                    vehicula tempus quis sit amet diam. Integer vulputate neque risus, sed lacinia eros egestas non.
                    Maecenas nec rutrum risus, vel suscipit quam. In quis enim nec lacus cursus maximus quis et mi.
                    Maecenas
                    faucibus ullamcorper vestibulum. Maecenas eget magna dolor. Nam cursus ac sem vel efficitur.</p>

                <p>Sed sit amet ipsum sit amet augue aliquam venenatis. Phasellus lacinia mi nec tristique iaculis.
                    Mauris
                    eu auctor magna, a efficitur augue. Pellentesque auctor enim risus. Aliquam erat volutpat. Fusce sit
                    amet nibh dolor. Maecenas molestie libero pharetra pretium faucibus. Pellentesque fringilla, lorem
                    non
                    laoreet dapibus, nisl neque bibendum justo, a auctor dui velit et nunc. Donec sit amet est vel lorem
                    pulvinar dignissim. Maecenas eu tempus neque, vel ultricies lorem. Sed dapibus fringilla vehicula.
                    Quisque sit amet magna quis leo tristique convallis. Aliquam sed iaculis libero.</p>

                <p>Sed posuere ex quam, ac semper libero auctor sed. Integer posuere rhoncus enim vel semper. Duis
                    ullamcorper fermentum dui in elementum. Quisque aliquam enim quis justo lacinia, vitae sollicitudin
                    nulla posuere. Integer sed lobortis lacus, eu mollis sapien. Vestibulum eget leo sodales, malesuada
                    risus vitae, porta arcu. Aenean pulvinar mollis urna, eget efficitur est porta et. Mauris efficitur,
                    lorem non tincidunt sodales, ipsum lectus faucibus est, eget finibus sem risus eget risus. Mauris ut
                    eleifend ex, fermentum semper eros. Proin pharetra nibh eu tortor lacinia feugiat. Fusce volutpat
                    finibus metus a hendrerit. Pellentesque non ante varius nisl porta tincidunt.</p>

                <p>Suspendisse facilisis ante diam, vel elementum tortor faucibus id. Phasellus malesuada diam ut lacus
                    tristique, in vestibulum metus rhoncus. Sed molestie aliquam nisl, ac porttitor augue pretium eu.
                    Duis
                    eget dolor aliquam, pharetra diam ut, accumsan ante. Morbi et.</p>
            </div>);
    }
}

export default MetalContent
