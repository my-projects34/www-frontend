import React from "react";

class PopContent extends React.Component {
    render() {
        return (
            <div>
                <p>Pop music is a genre of popular music that originated in its modern forms in the US and the UK during
                    the mid-1950s. The terms "popular music" and "pop music" are often used interchangeably, although
                    both describe all music that is popular and that include many diverse styles. "Pop" and "rock" were
                    roughly synonymous terms until the late 1960s, when they became quite separated from each other.</p>

                <p>Although much of the music that appears on record charts is seen as pop music, the genre is
                    distinguished from chart music. Pop music often borrows elements from other styles such as urban,
                    dance, rock, Latin, and country; nevertheless, there are many key elements that define pop music.
                    Identifying factors usually include short to medium-length songs written in a basic format (often
                    the verse-chorus structure), as well as common use of repeated choruses, melodic tunes, and
                    hooks.</p>
                <p>David Hatch and Stephen Millward define pop music as "a body of music which is distinguishable from
                    popular, jazz, and folk musics". According to Pete Seeger, pop music is "professional music which
                    draws upon both folk music and fine arts music". Although pop music is seen as just the singles
                    charts, it is not the sum of all chart music. The music charts contain songs from a variety of
                    sources, including classical, jazz, rock, and novelty songs. As a genre, pop music is seen to exist
                    and develop separately. Therefore, the term "pop music" may be used to describe a distinct genre,
                    designed to appeal to all, often characterized as "instant singles-based music aimed at teenagers"
                    in contrast to rock music as "album-based music for adults".</p>
                <p>David Hatch and Stephen Millward define pop music as "a body of music which is distinguishable from
                    popular, jazz, and folk musics". According to Pete Seeger, pop music is "professional music which
                    draws upon both folk music and fine arts music". Although pop music is seen as just the singles
                    charts, it is not the sum of all chart music. The music charts contain songs from a variety of
                    sources,
                    including classical, jazz, rock, and novelty songs. As a genre, pop music is seen to exist and
                    develop
                    separately. Therefore, the term "pop music" may be used to describe a distinct genre, designed to
                    appeal to all, often characterized as "instant singles-based music aimed at teenagers" in contrast
                    to
                    rock music as "album-based music for adults".</p>
            </div>);
    }
}

export default PopContent
