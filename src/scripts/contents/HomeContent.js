import React from "react";

class HomeContent extends React.Component {
    render() {
        return (
            <div>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas dignissim ullamcorper ante, eget
                    lacinia tellus imperdiet eu. Sed ac erat orci. Nulla a cursus lorem, a congue magna. Nunc sit amet
                    enim
                    ac neque commodo aliquam a id ex. Nunc nec nulla et dolor lacinia placerat. Nunc eu dui scelerisque
                    ante
                    mattis convallis quis convallis tellus. Sed scelerisque efficitur justo malesuada vulputate. Morbi
                    ultrices sed quam ac aliquet. Vivamus et nulla a sapien sodales dictum. Quisque commodo ornare
                    neque.
                    Phasellus gravida mi et massa congue, ut sagittis sapien rutrum. Vivamus a dignissim urna, non
                    dapibus
                    dui. Proin iaculis, magna at laoreet dictum, nisl ante euismod lacus, non rutrum mauris mi sit amet
                    tortor. Donec mattis fringilla velit varius auctor. Aenean a sapien lorem.</p>

                <p>Curabitur ac ligula pulvinar, mattis ipsum a, accumsan massa. Ut elementum odio nec augue convallis,
                    vel
                    iaculis justo consectetur. Curabitur eget consequat quam. Maecenas pharetra rhoncus odio eget
                    pellentesque. Maecenas id mi vitae sapien consequat consequat. Interdum et malesuada fames ac ante
                    ipsum
                    primis in faucibus. Praesent in convallis metus. Proin faucibus fringilla arcu consectetur porta.
                    Fusce
                    dictum, lorem nec sodales interdum, lorem ligula varius turpis, ut varius metus nibh ac ante. Proin
                    fringilla erat at lectus efficitur, a laoreet odio tempor. Ut fermentum mauris non molestie
                    condimentum.
                    Aenean ornare nunc a tortor pellentesque finibus.</p>

                <p>Donec tristique erat vitae efficitur pulvinar. Pellentesque enim lorem, imperdiet a mi ac, aliquam
                    tincidunt nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis
                    egestas. Nullam congue tincidunt magna eget rhoncus. Ut malesuada, erat a efficitur facilisis, magna
                    elit commodo justo, vestibulum porttitor felis augue at mi. Nam lacinia, mi fringilla dapibus
                    ornare,
                    felis sem maximus purus, sit amet malesuada elit metus sit amet velit. Nam non posuere ante. Duis
                    nec
                    nisl et neque auctor aliquam. Proin at ornare nunc. Sed justo augue, bibendum quis mollis non,
                    commodo
                    et quam. Etiam ac placerat velit, in convallis dolor. Suspendisse non dui ipsum.</p>

                <p>Morbi condimentum rutrum erat ac malesuada. Sed ut erat ornare, consequat lacus id, auctor enim. Cras
                    mi
                    augue, tincidunt non scelerisque vitae, elementum scelerisque risus. Maecenas eu nibh quis mi
                    venenatis
                    tincidunt convallis eu massa. Suspendisse tristique, orci eu viverra placerat, dolor diam
                    condimentum
                    risus, nec dictum eros odio ut quam. Sed et rutrum mi. Duis eu enim ac ante suscipit convallis.
                    Curabitur auctor ultricies ipsum. Curabitur dapibus vestibulum leo in ornare. Curabitur quis arcu eu
                    elit accumsan feugiat. Sed non consectetur dolor. Sed eleifend eros vel turpis mollis, ac consequat
                    neque tincidunt. Maecenas nec nunc vel ligula tincidunt commodo. Donec metus tortor, molestie sed
                    convallis at, tincidunt a enim. Nulla rhoncus non nunc id pellentesque.</p>

                <p>Sed semper nunc sem, eget cursus lectus mattis imperdiet. Donec imperdiet at metus a laoreet. Vivamus
                    posuere arcu vitae gravida dignissim. Pellentesque convallis odio dolor, eu feugiat risus iaculis
                    vel.
                    Aenean fermentum mauris sed lorem dapibus, ac rhoncus felis commodo. Vestibulum ante ipsum primis in
                    faucibus orci luctus et ultrices posuere cubilia Curae; Nam at fringilla ante. Ut egestas mattis
                    nunc ac
                    laoreet. Nam vel sodales odio. Fusce sollicitudin turpis at metus porttitor vestibulum. Ut a eros
                    maximus, pellentesque neque sit amet, aliquam purus. Aenean vel nisl risus.</p>

                <p>Curabitur sed ipsum lectus. Nulla dignissim orci lacus, et elementum ante volutpat eget. Quisque id
                    iaculis metus. Vestibulum sed imperdiet orci, et consectetur mi. Quisque efficitur imperdiet auctor.
                    Aenean facilisis sem quam, quis cursus nisi rutrum tincidunt. Integer sit amet lacus mauris. Aliquam
                    lacinia placerat magna, nec convallis lacus ullamcorper vitae. Phasellus fringilla tristique magna a
                    euismod. Etiam rutrum purus nec elementum auctor.</p>

                <p>Morbi maximus tincidunt felis et efficitur. Mauris vel sapien quis purus molestie sollicitudin. Ut
                    rhoncus elit eros, id consectetur tellus fringilla at. Cras vulputate magna sapien, lacinia interdum
                    quam tincidunt ac. Nunc maximus, nisl nec rutrum lacinia, purus magna feugiat ligula, quis
                    ullamcorper
                    mi est a leo. Vestibulum arcu est, fringilla et nisl eu, lobortis efficitur lacus. Nunc dui dolor,
                    fermentum nec neque non, sagittis bibendum augue. Quisque nisi mauris, vulputate eget nisi ac,
                    dapibus
                    aliquet leo. Donec vel turpis at turpis lobortis accumsan sed vitae quam. Vivamus efficitur nisl a
                    accumsan rutrum. Maecenas pulvinar a neque at ullamcorper. Etiam commodo, arcu eget ornare
                    facilisis,
                    lacus justo eleifend diam, sed hendrerit lacus velit sed sapien.</p>

                <p>Phasellus aliquam ligula nec egestas congue. Integer at cursus risus. Morbi nec sem lectus. Vivamus
                    vitae sem efficitur, scelerisque diam eu, pellentesque metus. In hac habitasse platea dictumst.
                    Donec
                    vehicula erat nec turpis faucibus vehicula. Sed suscipit posuere urna id luctus. Donec viverra,
                    augue
                    commodo euismod mattis, augue magna molestie magna, tincidunt bibendum justo quam sit amet lectus.
                    Morbi
                    ultricies, nibh at facilisis sodales, metus nisl dictum leo, non imperdiet nisi nisi vitae lorem.
                    Nulla
                    in auctor nulla, vitae pretium lorem. Duis eu lectus convallis, interdum urna et, consectetur massa.
                    Aliquam in urna aliquam, pulvinar orci et, tempus eros.</p>

                <p>Ut mauris tellus, ultricies eu molestie at, eleifend nec neque. Integer ut ligula felis. Nulla sed
                    dignissim mauris, vestibulum pretium quam. Nulla pharetra, enim vitae convallis vestibulum, ante
                    felis
                    consectetur diam, sed malesuada velit nunc vitae ipsum. Proin ante arcu, cursus eget ex nec,
                    faucibus
                    dictum arcu. In efficitur nibh vitae purus vulputate venenatis sed vel diam. Vivamus sodales eros
                    orci,
                    ac dignissim sapien rutrum et.</p>

                <p>Quisque nec risus sed nunc consectetur facilisis a sed elit. Praesent laoreet dui rutrum leo
                    vestibulum
                    commodo. In hac habitasse platea dictumst. Pellentesque habitant morbi tristique senectus et netus
                    et
                    malesuada fames ac turpis egestas. Nullam vitae ligula non massa ultrices finibus sit amet quis
                    sapien.
                    Aliquam at tempor quam, volutpat ornare augue. Pellentesque et est lobortis, pulvinar lacus sit
                    amet,
                    accumsan quam.</p>

                <p>Suspendisse potenti. Nulla bibendum elit ut dui faucibus, nec porta est convallis. Proin sodales
                    convallis turpis eget fermentum. Ut ac lectus pulvinar, gravida eros quis, imperdiet turpis. Morbi
                    aliquet rhoncus eros, quis malesuada nunc hendrerit in. Nullam vulputate tempor justo, at porttitor
                    erat
                    aliquet et. Interdum et malesuada fames ac ante ipsum primis in faucibus. Suspendisse potenti. Donec
                    viverra, elit a efficitur pellentesque, lorem sem vulputate arcu.</p>
            </div>);
    }
}

export default HomeContent
