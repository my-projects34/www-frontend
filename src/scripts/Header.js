import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import headerImage from "../images/header.gif"
import "../styles/style.css"


class Header extends React.Component {
    render() {
        return (
            <div>
                {/* eslint-disable-next-line jsx-a11y/img-redundant-alt */}
                <img src={headerImage} className="header" alt={"Problem with loading the picture"}/>
            </div>
        )
    }
}

export default Header
